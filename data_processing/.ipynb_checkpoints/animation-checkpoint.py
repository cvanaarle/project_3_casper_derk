import numpy as np
from matplotlib import animation
from matplotlib import pyplot as plt

def ani_init(L, x):
    ln, = plt.plot([], [], 'r', animated=True)
    ax.set_xlim(0, L)
    ax.set_ylim(0, L)
    ln.set_data(x[:,0,0], x[:,1,0])
    return ln

def ani_update(frame,x):
	ln.set_data(x[:,0,frame], x[:,1,frame])
	return ln

def ani_make(x, L, T, T_frames):
    fig =  plt.plot()
    f = np.linspace(0, T, T_frames)
    ani = FuncAnimation(fig, ani_update, frames=f, init_func=ani_init, blit=True, interval = T/T_frames ,repeat=False)
    mywriter = FFMpegFileWriter(fps=25,codec="libx264")
    ani.save("test.mp4", writer=mywriter)
    

L = 3
x = [[1,2],[1.3,2],[1.6,2]]
T = 3
T_frames = 3
ani_make(x,L,T,T_frames)