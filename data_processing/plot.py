import PyQt5
import matplotlib
matplotlib.use("Agg")
import numpy as np
import matplotlib.pyplot as plt


def velocity_plot(v_mean,T,h,Lx,density):
    t = np.arange(0.0, T+1, 1)*h
    plt.figure()
    plt.plot(t, v_mean, 'b')
    plt.ylabel('v')
    plt.xlabel('t') #[$(m\sigma^2/\epsilon)^{1/2}$]
    #plt.title('Energy per particle')
    #plt.gca().legend(('$E_{kin}$','$E_{pot}$','$E_{tot}$'))
    plt.savefig('animations/Lx_%s_density_%0.2f_velocityplot.png' %(Lx,density))