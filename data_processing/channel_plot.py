import PyQt5
import matplotlib
matplotlib.use("Agg")
import numpy as np
import matplotlib.pyplot as plt


def channel_plot(x_channel,v_channel,Lx,lx,bins,i,f, density):
    x = np.linspace((Lx-lx)/2, (Lx+lx)/2, bins+1)
    v = np.zeros(bins)
    for m in range(bins):
        xbin = np.where(np.logical_and(np.greater(x_channel[int(0.8*f):f+1,:],x[m]) , np.less(x_channel[int(0.8*f):f+1,:], x[m+1])) , 1 , 0)
        v[m] = np.sum(xbin*v_channel[int(0.8*f):f+1,:])/(np.sum(xbin)+1)
        
    plt.figure()
    plt.plot(x[0:bins]+(x[1]-x[0])/2, v, 'b')
    plt.ylabel('v')
    plt.xlabel('x') #[$(m\sigma^2/\epsilon)^{1/2}$]
    #plt.title('Energy per particle')
    #plt.gca().legend(('$E_{kin}$','$E_{pot}$','$E_{tot}$'))
    plt.savefig('animations/Lx_%s_density_%0.2f_velocityprofile.png' %(Lx,density))