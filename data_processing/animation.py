"""
Makes an animation of the positions of all particles

input:
x : positions matrix of all particles at all times
L : size of domain
f : amount of animation frames
r : radi of particles

output:
gif saved in main folder

"""


import PyQt5
import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import sys

def animate(x,x_bulk,F,Lx,Ly,lx,ly,f,r,n, density):
    fig = plt.figure()
    ax = fig.add_axes([0, 0, 1, 1], projection= None, aspect="equal")
    ax.axis('off')
    ax.set_xlim((0, Lx))
    ax.set_ylim((0, Ly))
    # drawing the boundaries
    plt.plot([0, Lx], [0, 0], color='k', linestyle='-', linewidth=2)
    plt.plot([0, Lx], [Ly, Ly], color='k', linestyle='-', linewidth=2)
    plt.plot([0, 0], [0, (Ly-ly)/2], color='k', linestyle='-', linewidth=2)
    plt.plot([Lx, Lx], [0, (Ly-ly)/2], color='k', linestyle='-', linewidth=2)
    plt.plot([0, 0], [Ly-(Ly-ly)/2, Ly], color='k', linestyle='-', linewidth=2)
    plt.plot([Lx, Lx], [Ly-(Ly-ly)/2, Ly], color='k', linestyle='-', linewidth=2)
    plt.plot([(Lx-lx)/2, (Lx-lx)/2], [(Ly-ly)/2, Ly-(Ly-ly)/2], color='k', linestyle='-', linewidth=2)
    plt.plot([Lx-(Lx-lx)/2, Lx-(Lx-lx)/2], [(Ly-ly)/2, Ly-(Ly-ly)/2], color='k', linestyle='-', linewidth=2)
    plt.plot([0, (Lx-lx)/2], [(Ly-ly)/2, (Ly-ly)/2], color='k', linestyle='-', linewidth=2)
    plt.plot([Lx-(Lx-lx)/2, Lx], [(Ly-ly)/2, (Ly-ly)/2], color='k', linestyle='-', linewidth=2)
    plt.plot([0, (Lx-lx)/2], [Ly-(Ly-ly)/2, Ly-(Ly-ly)/2], color='k', linestyle='-', linewidth=2)
    plt.plot([Lx-(Lx-lx)/2, Lx], [Ly-(Ly-ly)/2, Ly-(Ly-ly)/2], color='k', linestyle='-', linewidth=2)
    
    # boundary colors
    #colors = np.transpose(np.where(x_bulk[0,:,0]==0,1,0))
    
    # pedestrian colors
    colors = np.concatenate(([0.8,0,0]*np.ones([int(n/2),3]), [0,0,0.8]*np.ones([int(n/2),3])), axis=0)
    
    points = ax.scatter(x[0, :, 0], x[0, :, 1],s=(640*r/(Ly))**2,c='None',edgecolors=colors)
    #points.set_array(colors)
    #points = ax.quiver(x[0,:,0], x[0,:,1],F[0,:,0],F[0,:,1])
    
    def update(i):
        x_new = np.transpose(np.vstack((x[i, :, 0], x[i, :, 1])))
        points.set_offsets(x_new)
        
        #colors_new = np.transpose(np.where(x_bulk[i,:,0]==0,1,0))
        #points.set_array(colors_new)
        
        fig.canvas.draw()
        return points
    
    def generate_points():
        return points
    
    ani = animation.FuncAnimation(fig, update, init_func= generate_points, frames=f, interval=300)
    ani.save('animations/Lx_%s_density_%0.2f_animation.gif' %(Lx,density), writer='pillow', fps=4);
