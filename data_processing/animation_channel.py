"""
Makes an animation of the positions of all particles

input:
x : positions matrix of all particles at all times
L : size of domain
f : amount of animation frames
r : radi of particles

output:
gif saved in main folder

"""


import PyQt5
import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import sys

def animate_channel(x,v,Lx,Ly,lx,ly,f,r,n):
    fig = plt.figure()
    ax = fig.add_axes([0, 0, 1, 1], projection= None)
    ax.axis('off')
    ax.set_xlim((0, Lx))
    ax.set_ylim((-1000,1000))
    
    plt.plot([0, Lx], [0, 0], color='k', linestyle='-', linewidth=2)
    # boundary colors
    #colors = np.transpose(np.where(x_bulk[0,:,0]==0,1,0))
    
    # pedestrian colors
    colors = np.concatenate(([0.8,0,0]*np.ones([int(n/2),3]), [0,0,0.8]*np.ones([int(n/2),3])), axis=0)
    
    points = ax.scatter(x[0, :], v[0, :],s=2,c='None',edgecolors=colors)
    #points.set_array(colors)
    #points = ax.quiver(x[0,:,0], x[0,:,1],F[0,:,0],F[0,:,1])
    
    def update(i):
        x_new = np.transpose(np.vstack((x[i, :], v[i, :])))
        points.set_offsets(x_new)
        
        #colors_new = np.transpose(np.where(x_bulk[i,:,0]==0,1,0))
        #points.set_array(colors_new)
        
        fig.canvas.draw()
        return points
    
    def generate_points():
        return points
    
    ani = animation.FuncAnimation(fig, update, init_func= generate_points, frames=f, interval=300)
    ani.save('animations/animation_channel.gif', writer='pillow', fps=4);

def animate_channel_plot(x_channel,v_channel,Lx,lx,bins,f):
    #fig = plt.figure()
    
    
    #x = np.linspace((Lx-lx)/2, (Lx+lx)/2, bins+1)
    v = np.zeros(bins)
    
    #ax = plt.axes(xlim=((Lx-lx)/2, (Lx+lx)/2), ylim=(-20, 20))
    #line, = ax.plot([], [], lw=2)

    
    #for m in range(bins):
     #   xbin = np.where(np.logical_and(np.greater(x_channel[0,:],x[m]) , np.less(x_channel[0,:], x[m+1])) , 1 , 0)
      #  v[m] = np.sum(xbin*v_channel[0,:])/(np.sum(xbin)+1)
        
    #line = ax.plot([], [], 'b')
    #plt.ylabel('v')
    #plt.xlabel('x') 
    
    
    fig = plt.figure()
    ax = plt.axes(xlim=((Lx-lx)/2, (Lx+lx)/2), ylim=(-20, 20))
    line, = ax.plot([], [], lw=2)

    def update(q):
        x = np.linspace((Lx-lx)/2, (Lx+lx)/2, bins+1)
        #y = np.sin(2 * np.pi * (x - 0.01 * q))
        #line.set_data(x, y)
        #print(q)
        for m in range(bins):
            xbin = np.where(np.logical_and(np.greater(x_channel[q+1,:],x[m]) , np.less(x_channel[q+1,:], x[m+1])) , 1 , 0)
            v[m] = np.sum(xbin*v_channel[q+1,:])/(np.sum(xbin)+1)
            
        
        line.set_data(x[0:bins]+(x[1]-x[0])/2,v)
        return line
    
    def generate_points():
        line.set_data([], [])
        return line
    
    ani = animation.FuncAnimation(fig, update, init_func= generate_points, frames=f, interval=300)
    ani.save('animations/animation_channel_plot.gif', writer='pillow', fps=4);
