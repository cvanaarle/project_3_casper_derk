"""
---INITIAL CONDITIONS---

n : number of particles
L : box size
h : time step size
T : total time steps
f : number of frames for animation

R_mean : mean radius of particles
R_var : variance in radius of particles
F_mean : mean starting force of particles
F_var : variance in starting force of particles
springconstant : spring constant of the particles



vsc : rescaled_viscosity



D : dimensions (keep at 2!)
x : positions of particles
r : radius of particles
"""

import numpy as np



m = 5
n = m**2
h = 0.001
T = 200000
f = 250
D = 2
bins = 30
density = 20/20

simulation_type = "street"


if simulation_type == "street":
    n = int(m*40*density)
    
    R_mean = 1
    R_var = 0.1
    F1 = -10
    F2 = 10
    springconstant = 100
    springconstant_wall = 1000
    vsc = 1
    
    Lx = 2*m
    lx = 2*m
    Ly = 80*R_mean
    ly = 10
    
    r = np.random.normal(R_mean, R_var, n)
    r_neighbour = 2.7 * np.mean(r)
    F = 0# = np.random.normal(F_mean, F_var, (n,D))
    
    x_init = np.zeros([n,D])
    x_init0 = np.linspace(R_mean,Lx-R_mean,m)
    x_init1 = np.linspace(R_mean, 0.5*Ly*(density-(1/20)) + 0.5*R_mean ,int(20*density))
    x_init1 = np.append(Ly-x_init1, x_init1)
    x_init[:,0] = np.tile(x_init0,int(40*density))
    x_init[:,1] = np.repeat(x_init1, m)
    
    r_neighbour = 2.7 * np.mean(r)
    Fy = np.concatenate((F1*np.ones([1,(int(n/2))]),F2*np.ones([1,(int(n/2))])), axis=1)#random.normal(F_mean1, F_var, (int(n/2))), np.random.normal(F_mean2, F_var, (int(n/2)))), axis=0)
    Fy = np.squeeze(Fy)#[np.newaxis]
    Fx = np.zeros([1,n])
    Fx = np.squeeze(Fx)
    F_drift = np.vstack((Fx,Fy))
    F_drift = np.swapaxes(F_drift,0,1)
    
    F = 0

if simulation_type == "nor":
    # normal box constants
    
    R_mean = 1
    R_var = 0.1
    F_mean = 0
    F_var = 1
    springconstant = 1
    
    springconstant_wall = 0
    
    vsc = 50
    
    Lx = 150
    Ly = 100
    lx = 0
    ly = 0
    
    r = np.random.normal(R_mean, R_var, n)
    r_neighbour = 2.7 * np.mean(r)
    F = 0# = np.random.normal(F_mean, F_var, (n,D))
    
    x_init = np.zeros([n,D])
    x_init0 = np.linspace((Lx/2-1*R_mean*(m-1)),(Lx/2+1*R_mean*(m-1)),m)
    x_init[:,0] = np.tile(x_init0, m)+np.random.normal(0,0.01*R_mean,(1,n))
    x_init[:,1] = np.repeat(x_init0, m)+np.random.normal(0,0.1*R_mean,(1,n))
    
if simulation_type == "ped":
    # Pedestrian domain constants
    n = 2*m**2
    
    lx = 2*m+10
    ly = 50
    Lx = 2*m+10
    Ly = 2*Lx+ly
    
    R_mean = 1
    R_var = 0.1
    F1 = -10
    F2 = 10
    springconstant = 100
    
    springconstant_wall = 1000
    
    vsc = 1
    
    
    x_init = np.zeros([n,D])
    x_init0 = np.linspace((Lx/2-1*R_mean*(m-1)),(Lx/2+1*R_mean*(m-1)),m)
    y_init1 = np.linspace(((Ly-Lx/2)-1*R_mean*(m-1)),((Ly-Lx/2)+1*R_mean*(m-1)),m)
    y_init2 = x_init0
    y_init0 = np.concatenate((y_init1, y_init2), axis=None)
    
    x_init[:,0] = np.tile(x_init0, 2*m)+np.random.normal(0,0.01*R_mean,(1,n))
    x_init[:,1] = np.repeat(y_init0, m)+np.random.normal(0,0.1*R_mean,(1,n))

    
   
    
    r = np.random.normal(R_mean, R_var, n)
    r_neighbour = 2.7 * np.mean(r)
    Fy = np.concatenate((F1*np.ones([1,(int(n/2))]),F2*np.ones([1,(int(n/2))])), axis=1)#random.normal(F_mean1, F_var, (int(n/2))), np.random.normal(F_mean2, F_var, (int(n/2)))), axis=0)
    Fy = np.squeeze(Fy)#[np.newaxis]
    Fx = np.zeros([1,n])
    Fx = np.squeeze(Fx)
    F_drift = np.vstack((Fx,Fy))
    F_drift = np.swapaxes(F_drift,0,1)
    
    F = 0
    
    
    