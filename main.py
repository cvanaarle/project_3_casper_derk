import numpy as np
import sys
import time
from tqdm import tqdm

from initial_conditions import T, F, f, Lx, Ly, lx, ly, r, r_neighbour, n,h, bins, density
from simulation.adaptions import *
from simulation.data_functions import *
from data_processing.animation import animate
from data_processing.animation_channel import *
from data_processing.plot import *
from data_processing.channel_plot import *

x, F, x_boundary, v_mean,x_channel,v_channel = data_initialisation()


for k in tqdm(range(f)):
    
    x_loop, F_loop, x_boundary_loop, v_mean_loop = data_init_loop(k,x,F,x_boundary,v_mean)
    for t in range(int(T/f)):
        f_boundary = boundary_force(x_loop[t])
        
        x_loop, F_loop, x_boundary_loop , v_mean_loop = data_write_loop(t,x_loop,F_loop,f_boundary, x_boundary_loop, v_mean_loop)
        
    x, F, x_boundary , v_mean,x_channel,v_channel = data_write(k,x,F,f_boundary,x_boundary,v_mean,x_loop,x_boundary_loop,F_loop,v_mean_loop,x_channel,v_channel)
    
# generate animation
animate(x,x_boundary,F,Lx, Ly, lx, ly ,f+1,r,n, density)
i = 1
#velocity_plot(v_mean,f,h*int(T/k),i)

channel_plot(x_channel,v_channel,Lx,lx,bins,i,f,density)


