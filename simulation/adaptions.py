"""
---functions that are used to calculate the next positions---

velocity: Takes in a force and rescales it to a velocity
position_update: Calculates the new position based the current position and the force
force_update: Calculates the new force
overlap_force: Checks whether particles overlap


"""

from initial_conditions import h, r, vsc, n, D, springconstant, r_neighbour, F_drift, Lx, Ly, lx, ly, springconstant_wall

import numpy as np


def velocity(F):
    v = F / r[:,np.newaxis] * vsc
    return v

def position_update(x,F):
    xnew = x + h*velocity(F)
    xnew[:,1] = xnew[:,1]%Ly
    return xnew

def force_update(F,x,f_boundary):
    Fnew =  0.5*F + 0.5*F_drift + overlap_force(x) + 0.5*(np.random.rand(n,D)-0.5) + boundary_domain(x)# + boundary_lane(x) + 0.1*restoring_force(x)# - 0.1*seek_center(x) 0.5*f_boundary +
    return Fnew

def overlap_force(x):
    x_rel = x[np.newaxis] - np.swapaxes(x[np.newaxis],0,1)   #gives relative positions
    distance = np.sqrt((x_rel**2).sum(axis=2))          #gives relative distances (sqrt(x^2 + y^2))
    overlap = distance - r[np.newaxis] - np.swapaxes(r[np.newaxis],0,1) #calculates how far the particles are overlapping
    distance_nn_inv = np.zeros((n,n))
    np.divide(1,distance,out=distance_nn_inv, where=(np.logical_and(np.not_equal(distance,0),np.less(overlap,0)))) #takes the inverse distance for where the particles are overlapping
    x_rel_norm = x_rel * np.swapaxes(distance_nn_inv[np.newaxis],0,2) #gives the relative positions normalised to the distances (x^2 + y^2 = 1 (unit direction vector))
    force = (springconstant * np.swapaxes(overlap[np.newaxis],0,2) * x_rel_norm).sum(axis=1) #takes the unit direction vector, multiplies it with the overlap times a constant to simulate hooke's law
    return force
"""
def boundary_force(x):
    x_rel = x[np.newaxis] - np.swapaxes(x[np.newaxis],0,1)   #gives relative positions
    distance = np.sqrt((x_rel**2).sum(axis=2))          #gives relative distances (sqrt(x^2 + y^2))
    neighbour_check = distance - r_neighbour # calculates if other particle is seen as a neighbour
    distance_nn_inv = np.zeros((n,n))
    np.divide(1,distance,out=distance_nn_inv, where=(np.logical_and(np.not_equal(distance,0),np.less(neighbour_check,0)))) #takes the inverse distance for where the particles are overlapping
    x_rel_norm = x_rel * np.swapaxes(distance_nn_inv[np.newaxis],0,2) #gives the relative positions normalised to the distances (x^2 + y^2 = 1 (unit direction vector))
    x_sum_vector = np.sum(x_rel_norm, axis = 1) # sums normalized neighbour vectors into one overall contribution
    x_sum_matrix = np.repeat(x_sum_vector[np.newaxis], n, axis=0)
    x_innerproduct = np.sum((x_sum_matrix * x_rel_norm),axis = 2)
    x_onboundary = np.max(x_innerproduct, axis =1)
    
    force_b = np.zeros((n,2))
    x_sum_vector_length = np.sqrt(x_sum_vector[:,0]**2+x_sum_vector[:,1]**2)
    np.divide(x_sum_vector,np.swapaxes(np.repeat(x_sum_vector_length[np.newaxis],2,axis=0),0,1),out=force_b, where= np.swapaxes(np.repeat(x_onboundary[np.newaxis],2,axis=0)>=0,0,1)) #takes the inverse distance for where the particles are overlapping
    
    
    
    #print('x_sum_matrix\n',x_sum_matrix)
    #print('x_rel_norm\n',x_rel_norm)
    #print('x_innerproduct\n',x_innerproduct)
    #print('x_boundary\n',x_boundary)
    #print('force_b\n', force_b)
    return force_b
"""

def boundary_force(x):
    #print('x\n', x)
    x_rel = x[np.newaxis] - np.swapaxes(x[np.newaxis],0,1)   #gives relative positions
    #print('x_rel\n', x_rel)
    distance = np.sqrt((x_rel**2).sum(axis=2))          #gives relative distances (sqrt(x^2 + y^2))
    #print('distance\n', distance)
    neighbour_check = distance - r_neighbour # calculates if other particle is seen as a neighbour
    #print('neighbour_check\n', neighbour_check)
    #print('r_neighbour\n',r_neighbour)
    distance_nn_inv = np.zeros((n,n))
    np.divide(1,distance,out=distance_nn_inv, where=(np.logical_and(np.not_equal(distance,0),np.less(neighbour_check,0)))) #takes the inverse distance for where the particles are overlapping
    #print('distance_nn_inv\n', distance_nn_inv)
    x_rel_norm = x_rel * np.swapaxes(distance_nn_inv[np.newaxis],0,2) #gives the relative positions normalised to the distances (x^2 + y^2 = 1 (unit direction vector))
    #print('x_rel_norm\n',x_rel_norm)
    x_sum_vector_old = np.sum(x_rel_norm, axis = 1) # sums normalized neighbour vectors into one overall contribution
    x_sum_vector = np.swapaxes(x_sum_vector_old[np.newaxis],0,1)
    #print('x_sum_vector\n',x_sum_vector)
    #print('x_sum_vector\n',np.shape(x_sum_vector))
    #print('x_sum_vector\n',np.swapaxes(x_sum_vector[np.newaxis],0,1))
    #print('x_sum_vector\n',np.shape(np.swapaxes(x_sum_vector[np.newaxis],0,1)))
    x_sum_matrix = np.repeat(x_sum_vector, n, axis=1)#np.repeat(x_sum_vector[np.newaxis], n, axis=0)
    #print('x_sum_matrix\n',x_sum_matrix)
    #print('x_sum_matrix\n',np.repeat(np.swapaxes(x_sum_vector[np.newaxis],0,1), n, axis=1))
    x_innerproduct = np.sum((x_sum_matrix * x_rel_norm),axis = 2)
    #print('x_sum_matrix * x_rel_norm\n',x_sum_matrix * x_rel_norm)
    #print('x_innerproduct\n',x_innerproduct)
    x_onboundary = np.min(x_innerproduct, axis =1)
    #print('x_onboundary\n',x_onboundary)
    force_b = np.zeros((n,2))
    x_sum_vector_length = np.sqrt(x_sum_vector[:,:,0]**2+x_sum_vector[:,:,1]**2)
    #print('x_sum_vector_length\n',x_sum_vector_length)
    #print((np.swapaxes(np.repeat(x_onboundary[np.newaxis],2,axis=0),0,1)))
    #print(np.swapaxes((np.repeat(np.swapaxes(x_sum_vector_length,0,1),2,axis=0)),0,1))
    np.divide(x_sum_vector_old,np.swapaxes((np.repeat(np.swapaxes(x_sum_vector_length,0,1),2,axis=0)),0,1),out=force_b, where= np.swapaxes(np.repeat(x_onboundary[np.newaxis],2,axis=0)<0,0,1)) #takes the inverse distance for where the particles are overlapping
    #print('x_sum_matrix\n',x_sum_matrix)
    #print('x_rel_norm\n',x_rel_norm)
    #print('x_innerproduct\n',x_innerproduct)
    #print('x_boundary\n',x_boundary)
    #print('force_b\n', force_b)
    return force_b

def bulk(x, f_boundary):
    x_bulk = np.where(np.abs(f_boundary)>0,0,1)
    #print('x_bulk\n', x_bulk)
    return x_bulk

def boundary(x, f_boundary):
    x_boundary = np.where(np.abs(f_boundary)>0,1,0)
    #print('x_boundary\n', x_boundary)
    return x_boundary














def seek_neighbour(x):
    force_str = 0.05
    x_rel = x[np.newaxis] - np.swapaxes(x[np.newaxis],0,1)   #gives relative positions
    distance = np.sqrt((x_rel**2).sum(axis=2))          #gives relative distances (sqrt(x^2 + y^2))
    overlap = distance - r[np.newaxis] - np.swapaxes(r[np.newaxis],0,1) #calculates how far the particles are overlapping
    
    no_overlap = np.where((np.where(np.logical_and(np.not_equal(distance,0),np.less(overlap,0)), overlap,0).sum(axis=0))==0,1,0) #1 for the particles with no overlap
    
    distance_nan = np.where(distance==0, np.nan, distance)
    distance_min = np.nanargmin(distance_nan,axis=1)
    
    min_distance_loc = np.where((np.ones([n,1])*np.arange(n))==(np.swapaxes((np.ones([n,1])*distance_min),0,1)),1,0)
    min_x_rel = np.swapaxes(min_distance_loc[np.newaxis],0,2) * x_rel

    force = force_str*min_x_rel.sum(axis=1)*np.swapaxes(no_overlap[np.newaxis],0,1)
    #print(force)
    return force

def seek_center(x):
    force_str = 1
    x_rel = x[np.newaxis] - np.swapaxes(x[np.newaxis],0,1)   #gives relative positions
    distance = np.sqrt((x_rel**2).sum(axis=2))          #gives relative distances (sqrt(x^2 + y^2))
    overlap = distance - r[np.newaxis] - np.swapaxes(r[np.newaxis],0,1) #calculates how far the particles are overlapping
    
    no_overlap = np.where((np.where(np.logical_and(np.not_equal(distance,0),np.less(overlap,0)), overlap,0).sum(axis=0))==0,1,0) #1 for the particles with no overlap
    force = - np.swapaxes(no_overlap[np.newaxis],0,1) * (x-50)
    return force





def boundary_domain(x):
    
    force = np.zeros([n,D])
    force[:,0] = np.where(x[:,0] < r,       springconstant_wall*(r-x[:,0]),         force[:,0])                 #left
    force[:,0] = np.where(x[:,0] > (Lx-r), springconstant_wall*((Lx-r)- x[:,0]),  force[:,0])    #right
    #force[:,1] = np.where(x[:,1] < r,       springconstant_wall*(r-x[:,1]),         force[:,1])                     #bottom
    #force[:,1] = np.where(x[:,1] > (Ly-r), springconstant_wall*((Ly-r) - x[:,1]), force[:,1])       #top
    return force

def boundary_lane(x):
    c1 = ((Ly+ly)-(Lx-lx))/2
    c2 = ((Ly-ly)+(Lx-lx))/2
    c3 = ((Ly+ly)+(Lx+lx))/2
    c4 = ((Ly-ly)-(Lx+lx))/2
    force = np.zeros([n,D])
    #left
    force[:,1] = np.where(np.logical_and.reduce((np.less(x[:,1], Ly/2) , np.greater(x[:,1],((Ly-ly)/2)-r), np.less(x[:,1]+x[:,0],c2))), springconstant_wall*((((Ly-ly)/2)-r)-x[:,1]) ,force[:,1])
    force[:,1] = np.where(np.logical_and.reduce((np.greater(x[:,1], Ly/2) , np.less(x[:,1],((Ly+ly)/2)+r), np.greater(x[:,1]-x[:,0],c1))), springconstant_wall*((((Ly+ly)/2)+r)-x[:,1]) ,force[:,1])
    force[:,0] = np.where(np.logical_and.reduce((np.less(x[:,0], ((Lx-lx)/2)+r) , np.greater(x[:,1]+x[:,0],c2), np.less(x[:,1]-x[:,0],c1))), springconstant_wall*((((Lx-lx)/2)+r)-x[:,0]) ,force[:,0])
    
    #right
    force[:,1] = np.where(np.logical_and.reduce((np.less(x[:,1], Ly/2) , np.greater(x[:,1],((Ly-ly)/2)-r), np.less(x[:,1]-x[:,0],c4))), springconstant_wall*((((Ly-ly)/2)-r)-x[:,1]) ,force[:,1])
    force[:,1] = np.where(np.logical_and.reduce((np.greater(x[:,1], Ly/2) , np.less(x[:,1],((Ly+ly)/2)+r), np.greater(x[:,1]+x[:,0],c3))), springconstant_wall*((((Ly+ly)/2)+r)-x[:,1]) ,force[:,1])
    force[:,0] = np.where(np.logical_and.reduce((np.greater(x[:,0], ((Lx+lx)/2)-r) , np.greater(x[:,1]-x[:,0],c4), np.less(x[:,1]+x[:,0],c3))), springconstant_wall*((((Lx+lx)/2)-r)-x[:,0]) ,force[:,0])
    
    return force

def restoring_force(x):
    force = np.zeros([n,D])
    
    force[:,0] = np.where(np.logical_and(np.less(x[:,1],((Ly-ly)/2)),np.less(x[:,0],((Lx-lx)/2)) ), 1 , force[:,0] )
    force[:,0] = np.where(np.logical_and(np.greater(x[:,1],((Ly+ly)/2)),np.less(x[:,0],((Lx-lx)/2)) ), 1 , force[:,0] )
    force[:,0] = np.where(np.logical_and(np.less(x[:,1],((Ly-ly)/2)),np.greater(x[:,0],((Lx+lx)/2)) ), -1 , force[:,0] )
    force[:,0] = np.where(np.logical_and(np.greater(x[:,1],((Ly+ly)/2)),np.greater(x[:,0],((Lx+lx)/2)) ), -1 , force[:,0] )
    return force
    