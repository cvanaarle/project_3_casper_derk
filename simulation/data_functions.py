from initial_conditions import T, n, D, x_init, f, Ly,ly
from simulation.adaptions import *

def data_initialisation():
    f_boundary = boundary_force(x_init)
    x = np.zeros([f+1,n,D])
    x_boundary = np.zeros([f+1,n,D])
    F = np.zeros([f+1,n,D])
    x[0][:][:] = x_init
    x_boundary[0][:][:] = boundary(x_init, f_boundary)
    F[0][:][:] = force_update(F[0],x_init,f_boundary)
    v_mean = np.zeros(f+1)
    v_mean[0] = np.mean(np.abs(velocity(F[0])[:,1]))
    x_channel = np.zeros([f+1,n])
    v_channel = np.zeros([f+1,n])
    
    return x, F, x_boundary, v_mean,x_channel,v_channel

def data_init_loop(k,x,F,x_boundary,v_mean):
    f_boundary_loop = boundary_force(x[k][:][:])
    x_loop = np.zeros([int(T/f)+1,n,D])
    x_boundary_loop = np.zeros([int(T/f)+1,n,D])
    F_loop = np.zeros([int(T/f)+1,n,D])
    x_loop[0][:][:] = x[k][:][:]
    x_boundary_loop[0][:][:] = x_boundary[k][:][:]
    F_loop[0][:][:] = F[k][:][:]
    v_mean_loop = np.zeros(int(T/f)+1)
    v_mean_loop[0] = v_mean[k]
    
    
    return x_loop, F_loop, x_boundary_loop, v_mean_loop

def data_write(k,x,F,f_boundary,x_boundary,v_mean,x_loop,x_boundary_loop,F_loop,v_mean_loop,x_channel,v_channel):
    #print(x[t][:][:])
    #print(position_update(x[t][:][:],F))
    x[k+1][:][:] = x_loop[-1]
    x_boundary[k+1][:][:] = x_boundary_loop[-1]
    F[k+1][:][:] = F_loop[-1]
    v_mean[k+1] = np.mean(v_mean_loop)
    x_channel[k+1][:][:],v_channel[k+1][:][:] = channel_visualisation(x[k+1][:][:],velocity(F[k+1][:][:]))
    return x, F, x_boundary, v_mean,x_channel,v_channel

def data_write_loop(t,x_loop,F_loop,f_boundary, x_boundary_loop, v_mean_loop):
    #print(x[t][:][:])
    #print(position_update(x[t][:][:],F))
    x_loop[t+1][:][:] = position_update(x_loop[t],F_loop[t])
    x_boundary_loop[t+1][:][:] = boundary(x_loop[t+1],f_boundary)
    F_loop[t+1][:][:] = force_update(F_loop[t],x_loop[t],f_boundary)
    vdirection = np.concatenate((-1*np.ones([1,(int(n/2))]),np.ones([1,(int(n/2))])), axis=1)
    vdirection = np.squeeze(vdirection)#[np.newaxis]
    v_mean_loop[t+1] = np.mean(vdirection*velocity(F_loop[t])[:,1])
    return x_loop, F_loop, x_boundary_loop, v_mean_loop

def channel_visualisation(x,v):
    ymax = 0.5*(Ly+0.2*ly)
    ymin = 0.5*(Ly-0.2*ly)
    x_channel = np.where(np.logical_and(np.less(x[:,1],ymax) , np.greater(x[:,1], ymin)) , x[:,0] , 0)
    v_channel = np.where(np.logical_and(np.less(x[:,1],ymax) , np.greater(x[:,1], ymin)) , v[:,1] , 0)
    return x_channel, v_channel